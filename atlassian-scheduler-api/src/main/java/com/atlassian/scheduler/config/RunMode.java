package com.atlassian.scheduler.config;

import com.atlassian.annotations.PublicApi;
import com.atlassian.scheduler.JobRunnerRequest;

/**
 * Represents how a Job will be run by the scheduler.
 * <p>
 * This mostly defines how a job will run in a clustered environment; however, it also affects
 * whether or not the job will survive a restart of the underlying application.
 * </p>
 */
@PublicApi
public enum RunMode {
    /**
     * The job is scheduled such that it will only run on one node of the cluster each time that it triggers.
     * <p>
     * Although jobs scheduled with this run mode must still register the {@code JobRunner} for the job on each restart,
     * the job's schedule will persist across restarts.
     * </p><p>
     * Note: this {@code RunMode} is susceptible to a race condition which may allow a job to run concurrently on
     * multiple nodes. In most cases this can be avoided by scheduling a job with a short inital delay (see
     * {@link com.atlassian.scheduler.SchedulerService#scheduleJob(JobId, JobConfig) scheduleJob}'s Javadoc for more
     * details). However, for long running jobs, a cluster-wide lock should be acquired inside the
     * {@link com.atlassian.scheduler.JobRunner#runJob(JobRunnerRequest) job}.
     * </p>
     */
    RUN_ONCE_PER_CLUSTER,

    /**
     * The job is scheduled such that it will apply only to this particular node of the cluster.
     * <p>
     * This job will not be persisted, and the job must be recreated if the application is restarted.
     * </p>
     */
    RUN_LOCALLY
}
