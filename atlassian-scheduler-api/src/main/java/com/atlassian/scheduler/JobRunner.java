package com.atlassian.scheduler;

import com.atlassian.annotations.PublicSpi;
import com.atlassian.scheduler.config.JobRunnerKey;

import javax.annotation.Nullable;

/**
 * Invoked by the {@link SchedulerService} when it is time for a scheduled job to run.
 * <p>
 * Application code should register the JobRunner on startup, and need do nothing on shutdown.
 * </p><p>
 * Plugins should register the JobRunner implementation at startup/plugin enabled, and
 * {@link SchedulerService#unregisterJobRunner(JobRunnerKey) unregister} the {@code JobRunner}
 * when the plugin is disabled.
 * </p>
 */
@PublicSpi
public interface JobRunner {
    /**
     * Called by the {@link SchedulerService} when it is time for a job to run.
     * The job is expected to perform its own error handling by catching exceptions
     * as appropriate and reporting an informative message using
     * {@link JobRunnerResponse#failed(String)}.
     *
     * @param request the information about the request that was supplied by the scheduler service
     * @return a {@link JobRunnerResponse} providing additional detail about the result of running the job.
     * The response is permitted to be {@code null}, which is treated as identical to
     * {@link JobRunnerResponse#success()}.
     */
    @Nullable
    JobRunnerResponse runJob(JobRunnerRequest request);
}
