/**
 * API classes related to the current status of a job or the results of running one.
 */
package com.atlassian.scheduler.status;