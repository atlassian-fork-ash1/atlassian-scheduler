package com.atlassian.scheduler.status;

import com.atlassian.annotations.PublicApi;
import com.atlassian.scheduler.JobRunnerResponse;
import com.atlassian.scheduler.config.RunMode;

/**
 * Indicates what the result was the last time that this job attempted to fire.
 */
@PublicApi
public enum RunOutcome {
    /**
     * The job ran {@link JobRunnerResponse#success() successfully}.
     */
    SUCCESS,

    /**
     * The job had no job runner registered when an attempt was made to fire it.
     * <p>
     * This likely means that the job is left over from an older version of the
     * application or from an add-on that is no longer installed.
     * </p><p>
     * <strong>Note</strong>: This outcome is only produced by the scheduler
     * implementations; it cannot be returned in a {@link JobRunnerResponse}.
     * </p>
     */
    UNAVAILABLE,

    /**
     * We did not start the job at the scheduled time because it was not ready.
     * <p>
     * Some possible causes include:
     * </p>
     * <ul>
     * <li>No job details could be found for that job ID.  It is possible that the
     * job was deleted right as it was scheduled to run.</li>
     * <li>The parameter map could not be reconstructed from its serialized form</li>
     * <li>The job has been registered with inconsistent run modes, probably with this
     * node using {@link RunMode#RUN_LOCALLY} and another node registering afterwards
     * using {@link RunMode#RUN_ONCE_PER_CLUSTER}</li>
     * <li>The job performed its own checks and explicitly returned a {@link JobRunnerResponse}
     * that indicated the job had been {@link JobRunnerResponse#aborted(String) aborted}.</li>
     * </ul>
     */
    ABORTED,

    /**
     * The job was started but it either threw an exception or returned a {@link JobRunnerResponse}
     * that indicated the job had {@link JobRunnerResponse#failed(String) failed}.
     */
    FAILED
}
