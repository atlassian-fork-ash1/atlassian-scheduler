package com.atlassian.scheduler;

import com.atlassian.annotations.PublicApi;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.status.RunDetails;

import javax.annotation.CheckForNull;

/**
 * Allows you to retrieve data about previous runs of a job.
 */
@PublicApi
public interface SchedulerHistoryService {
    /**
     * Returns the result of the most recent attempt to run this job.
     *
     * @param jobId the job ID of interest
     * @return the result information for the most recent run attempt, or {@code null} if there
     * is no recorded run history for this job
     */
    @CheckForNull
    RunDetails getLastRunForJob(JobId jobId);

    /**
     * Returns the result of the most recent successful run of this job.
     *
     * @param jobId the job ID of interest
     * @return the result information for the most recent run attempt, or {@code null} if there
     * is no successful result recorded for this job
     */
    @CheckForNull
    RunDetails getLastSuccessfulRunForJob(JobId jobId);
}
