package com.atlassian.scheduler.quartz1;

import com.atlassian.scheduler.core.tests.QuartzCronExpressionValidatorTest;
import org.junit.Before;
import org.junit.Test;

/**
 * @since v1.4
 */
public class Quartz1CronExpressionValidatorTest extends QuartzCronExpressionValidatorTest {
    @Before
    public void setUp() {
        validator = new Quartz1CronExpressionValidator();
    }

    // Unsupported in Quartz 1.x
    @Override
    @Test
    public void testLastFlagWithHyphenW() {
        assertInvalidFlagL("0 0 0 L-W 1 ?", 6);
    }

    // Unsupported in Quartz 1.x
    @Override
    @Test
    public void testSupportForLastMinusNumberForDayOfMonth() {
        assertInvalidFlagL("0 0 0 L-3 1 ?", 6);
        assertInvalidFlagL("0 0 0 L-4W 1 ?", 6);
    }
}

