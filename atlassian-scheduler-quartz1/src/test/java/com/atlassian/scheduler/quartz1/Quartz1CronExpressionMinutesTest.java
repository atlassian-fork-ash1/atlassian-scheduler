package com.atlassian.scheduler.quartz1;

import com.atlassian.scheduler.core.tests.CronExpressionMinutesTest;

/**
 * @since v1.5
 */
public class Quartz1CronExpressionMinutesTest extends CronExpressionMinutesTest {
    public Quartz1CronExpressionMinutesTest() {
        super(new Quartz1CronFactory());
    }
}
