package com.atlassian.scheduler.quartz1;

import com.atlassian.scheduler.core.tests.CronExpressionMonthTest;

/**
 * @since v1.5
 */
public class Quartz1CronExpressionMonthTest extends CronExpressionMonthTest {
    public Quartz1CronExpressionMonthTest() {
        super(new Quartz1CronFactory());
    }
}
