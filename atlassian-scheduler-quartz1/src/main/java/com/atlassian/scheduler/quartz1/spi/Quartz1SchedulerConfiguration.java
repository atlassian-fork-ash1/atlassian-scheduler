package com.atlassian.scheduler.quartz1.spi;

import com.atlassian.scheduler.core.spi.SchedulerServiceConfiguration;
import com.atlassian.scheduler.quartz1.Quartz1DefaultSettingsFactory;

import javax.annotation.Nonnull;
import java.util.Properties;

/**
 * Provides custom configuration settings for the Quartz 1.x scheduler service.
 *
 * @since v1.0
 */
public interface Quartz1SchedulerConfiguration extends SchedulerServiceConfiguration {
    /**
     * Returns custom properties for configuring the scheduler that will be used to run
     * local jobs.
     * <p>
     * <strong>WARNING</strong>: Since v1.3, the only property that provides a default
     * value is {@code org.quartz.scheduler.skipUpdateCheck}, which is always forced to
     * {@code true} regardless of the settings returned here.  To get the old defaults,
     * you can use {@link Quartz1DefaultSettingsFactory#getDefaultLocalSettings()}
     * as the starting point.
     * </p>
     */
    @Nonnull
    Properties getLocalSettings();

    /**
     * Returns custom properties for configuring the scheduler that will be used to run
     * clustered jobs.
     * <p>
     * <strong>WARNING</strong>: Since v1.3, the only property that provides a default
     * value is {@code org.quartz.scheduler.skipUpdateCheck}, which is always forced to
     * {@code true} regardless of the settings returned here.  To get the old defaults,
     * you can use {@link Quartz1DefaultSettingsFactory#getDefaultClusteredSettings()}
     * as the starting point.
     * </p>
     */
    @Nonnull
    Properties getClusteredSettings();
}
