package com.atlassian.scheduler.quartz1;

import com.atlassian.scheduler.cron.CronExpressionValidator;
import com.atlassian.scheduler.cron.CronSyntaxException;
import org.quartz.CronExpression;

import java.text.ParseException;
import java.util.Locale;

import static com.atlassian.scheduler.core.util.QuartzParseExceptionMapper.mapException;

/**
 * @since v1.4
 */
public class Quartz1CronExpressionValidator implements CronExpressionValidator {
    @Override
    public boolean isValid(String cronExpression) {
        return CronExpression.isValidExpression(cronExpression);
    }

    @Override
    public void validate(String cronExpression) throws CronSyntaxException {
        try {
            //noinspection ResultOfObjectAllocationIgnored
            new CronExpression(cronExpression);
        } catch (ParseException pe) {
            throw mapException(cronExpression.toUpperCase(Locale.US), pe);
        }
    }
}
