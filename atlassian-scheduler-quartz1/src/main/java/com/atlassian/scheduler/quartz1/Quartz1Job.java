package com.atlassian.scheduler.quartz1;

import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.RunMode;
import com.atlassian.scheduler.core.AbstractSchedulerService;
import com.atlassian.scheduler.core.JobLauncher;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.spi.TriggerFiredBundle;

/**
 * Quartz 1.x {@code Job} that delegates to {@link JobLauncher}.
 *
 * @since v1.0
 */
public class Quartz1Job implements Job {
    private final JobLauncher jobLauncher;

    Quartz1Job(final AbstractSchedulerService schedulerService, final RunMode schedulerRunMode,
               final TriggerFiredBundle bundle) {
        final JobId jobId = JobId.of(bundle.getTrigger().getName());
        this.jobLauncher = new JobLauncher(schedulerService, schedulerRunMode, bundle.getFireTime(), jobId);
    }

    @Override
    public void execute(final JobExecutionContext context) throws JobExecutionException {
        jobLauncher.launch();
    }

    @Override
    public String toString() {
        return "Quartz1Job[jobLauncher=" + jobLauncher + ']';
    }
}

