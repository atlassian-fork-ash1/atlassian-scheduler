package com.atlassian.scheduler.quartz1;

import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.CronScheduleInfo;
import com.atlassian.scheduler.config.IntervalScheduleInfo;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.scheduler.core.spi.SchedulerServiceConfiguration;
import com.atlassian.scheduler.core.util.ParameterMapSerializer;
import com.atlassian.scheduler.cron.CronSyntaxException;
import org.quartz.CronTrigger;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;

import javax.annotation.Nonnull;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import static com.atlassian.scheduler.core.util.CronExpressionQuantizer.quantizeSecondsField;
import static com.atlassian.scheduler.core.util.QuartzParseExceptionMapper.mapException;
import static com.atlassian.scheduler.core.util.TimeIntervalQuantizer.quantizeToMinutes;
import static com.atlassian.scheduler.quartz1.Quartz1SchedulerFacade.QUARTZ_PARAMETERS_KEY;
import static com.atlassian.scheduler.quartz1.Quartz1SchedulerFacade.QUARTZ_TRIGGER_GROUP;
import static org.quartz.SimpleTrigger.REPEAT_INDEFINITELY;

/**
 * @since v1.0
 */
class Quartz1TriggerFactory {
    private final SchedulerServiceConfiguration config;
    private final ParameterMapSerializer parameterMapSerializer;

    Quartz1TriggerFactory(SchedulerServiceConfiguration config, ParameterMapSerializer parameterMapSerializer) {
        this.config = config;
        this.parameterMapSerializer = parameterMapSerializer;
    }


    public Trigger buildTrigger(final JobId jobId, final JobConfig jobConfig)
            throws SchedulerServiceException {
        final byte[] parameters = parameterMapSerializer.serializeParameters(jobConfig.getParameters());
        final Trigger trigger = buildTrigger(jobConfig.getSchedule());
        trigger.setGroup(QUARTZ_TRIGGER_GROUP);
        trigger.setName(jobId.toString());
        trigger.getJobDataMap().put(QUARTZ_PARAMETERS_KEY, parameters);
        return trigger;
    }

    public Trigger buildTrigger(Schedule schedule) throws SchedulerServiceException {
        switch (schedule.getType()) {
            case INTERVAL:
                return getSimpleTrigger(schedule.getIntervalScheduleInfo());
            case CRON_EXPRESSION:
                return getCronTrigger(schedule.getCronScheduleInfo());
        }
        throw new IllegalStateException("type=" + schedule.getType());
    }

    private static Trigger getSimpleTrigger(final IntervalScheduleInfo info) {
        final Date startTime = (info.getFirstRunTime() != null) ? info.getFirstRunTime() : new Date();
        final SimpleTrigger trigger = new SimpleTrigger();
        trigger.setStartTime(startTime);
        if (info.getIntervalInMillis() == 0L) {
            trigger.setRepeatCount(0);
        } else {
            trigger.setRepeatCount(REPEAT_INDEFINITELY);
            trigger.setRepeatInterval(quantizeToMinutes(info.getIntervalInMillis()));
        }
        return trigger;
    }

    private Trigger getCronTrigger(final CronScheduleInfo info) throws CronSyntaxException {
        try {
            final CronTrigger trigger = new CronTrigger();
            trigger.setCronExpression(quantizeSecondsField(info.getCronExpression()));
            trigger.setTimeZone(getTimeZone(info));
            return trigger;
        } catch (ParseException pe) {
            throw mapException(info.getCronExpression().toUpperCase(Locale.US), pe);
        }
    }

    @Nonnull
    private TimeZone getTimeZone(CronScheduleInfo info) {
        TimeZone timeZone = info.getTimeZone();
        if (timeZone == null) {
            timeZone = config.getDefaultTimeZone();
            if (timeZone == null) {
                timeZone = TimeZone.getDefault();
            }
        }
        return timeZone;
    }
}
