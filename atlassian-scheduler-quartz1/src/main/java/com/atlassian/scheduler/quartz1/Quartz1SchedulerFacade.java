package com.atlassian.scheduler.quartz1;

import com.atlassian.scheduler.SchedulerRuntimeException;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.RunMode;
import com.atlassian.scheduler.core.AbstractSchedulerService;
import com.atlassian.scheduler.quartz1.spi.Quartz1SchedulerConfiguration;
import io.atlassian.util.concurrent.LazyReference;
import com.google.common.base.Function;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Properties;
import java.util.function.Supplier;

import static com.atlassian.scheduler.config.RunMode.RUN_LOCALLY;
import static com.atlassian.scheduler.config.RunMode.RUN_ONCE_PER_CLUSTER;
import static com.google.common.collect.Lists.transform;
import static java.util.Arrays.asList;
import static java.util.Objects.requireNonNull;

/**
 * Wraps the quartz scheduler to make it work a bit closer to how we need it to.
 * Specifically, this hides Quartz's checked exceptions and provides the association
 * between our jobId/jobRunnerKey and the trigger and group names.
 */
class Quartz1SchedulerFacade {
    private static final Logger LOG = LoggerFactory.getLogger(Quartz1SchedulerFacade.class);

    static final String QUARTZ_JOB_GROUP = "SchedulerServiceJobs";
    static final String QUARTZ_TRIGGER_GROUP = "SchedulerServiceTriggers";
    static final String QUARTZ_PARAMETERS_KEY = "parameters";

    private final Supplier<Scheduler> quartzRef;

    private Quartz1SchedulerFacade(final Supplier<Scheduler> quartzRef) {
        this.quartzRef = quartzRef;
    }


    /**
     * Creates the local scheduler facade by wrapping a supplied Scheduler instance.
     */
    static Quartz1SchedulerFacade createLocal(final AbstractSchedulerService schedulerService,
                                              final Scheduler scheduler) throws SchedulerServiceException {
        requireNonNull(scheduler, "scheduler");
        return createFacade(schedulerService, scheduler, RUN_LOCALLY);
    }

    /**
     * Creates the clustered scheduler facade by wrapping a supplied Scheduler instance.
     */
    static Quartz1SchedulerFacade createClustered(final AbstractSchedulerService schedulerService,
                                                  final Scheduler scheduler) throws SchedulerServiceException {
        requireNonNull(scheduler, "scheduler");
        return createFacade(schedulerService, scheduler, RUN_ONCE_PER_CLUSTER);
    }

    /**
     * Creates a schedule scheduler facade by wrapping a supplied scheduler instance.
     */
    private static Quartz1SchedulerFacade createFacade(final AbstractSchedulerService schedulerService,
                                                       final Scheduler scheduler,
                                                       final RunMode runMode) throws SchedulerServiceException {
        try {
            configureScheduler(scheduler, schedulerService, runMode);
            final Supplier<Scheduler> quartzRef = () -> scheduler;
            return new Quartz1SchedulerFacade(quartzRef);
        } catch (SchedulerException se) {
            throw checked("Unable to configure the underlying Quartz scheduler", se);
        }
    }


    /**
     * Creates the local scheduler facade lazily using the supplied configuration.
     */
    static Quartz1SchedulerFacade createLocal(final AbstractSchedulerService schedulerService,
                                              final Quartz1SchedulerConfiguration config) throws SchedulerServiceException {
        requireNonNull(config, "config");
        final Properties localSettings = requireNonNull(config.getLocalSettings(), "config.getLocalSettings()");
        return createFacade(schedulerService, localSettings, RUN_LOCALLY);
    }

    /**
     * Creates the clustered scheduler facade lazily using the supplied configuration.
     */
    static Quartz1SchedulerFacade createClustered(final AbstractSchedulerService schedulerService,
                                                  final Quartz1SchedulerConfiguration config) throws SchedulerServiceException {
        requireNonNull(config, "config");
        final Properties clusteredSettings = requireNonNull(config.getClusteredSettings(), "config.getClusteredSettings()");
        return createFacade(schedulerService, clusteredSettings, RUN_ONCE_PER_CLUSTER);
    }

    /**
     * Creates a schedule scheduler facade lazily using the supplied configuration.
     */
    private static Quartz1SchedulerFacade createFacade(final AbstractSchedulerService schedulerService,
                                                       final Properties customConfig, final RunMode runMode)
            throws SchedulerServiceException {
        try {
            final Properties config = new Properties();
            for (String key : customConfig.stringPropertyNames()) {
                config.setProperty(key, customConfig.getProperty(key));
            }

            // JRA-23747 -- never allow Quartz to run its update check
            config.setProperty("org.quartz.scheduler.skipUpdateCheck", "true");

            final StdSchedulerFactory schedulerFactory = new StdSchedulerFactory(config);
            final Supplier<Scheduler> quartzRef = createQuartzRef(schedulerService, runMode, schedulerFactory);
            return new Quartz1SchedulerFacade(quartzRef);
        } catch (SchedulerException se) {
            throw checked("Unable to create the underlying Quartz scheduler", se);
        }
    }


    private static Supplier<Scheduler> createQuartzRef(final AbstractSchedulerService schedulerService,
                                                       final RunMode runMode,
                                                       final StdSchedulerFactory schedulerFactory) {
        return new LazyReference<Scheduler>() {
            @Override
            protected Scheduler create() throws Exception {
                // SCHEDULER-11: Quartz cares about the thread's CCL.  This makes sure that
                // the class loader that was used to construct the Quartz1SchedulerService
                // is set as the thread's CCL at the time the scheduler is lazily constructed.
                // otherwise, if a plugin is given direct access to Quartz scheduler before
                // the application initializes it, the plugin bundle's classloader could be
                // set as the thread's CCL, with unpleasant results like HOT-6239.
                final Thread thd = Thread.currentThread();
                final ClassLoader originalContextClassLoader = thd.getContextClassLoader();
                try {
                    thd.setContextClassLoader(schedulerService.getClass().getClassLoader());
                    final Scheduler quartz = schedulerFactory.getScheduler();
                    configureScheduler(quartz, schedulerService, runMode);
                    return quartz;
                } finally {
                    thd.setContextClassLoader(originalContextClassLoader);
                }
            }
        };
    }

    static void configureScheduler(final Scheduler quartz,
                                   final AbstractSchedulerService schedulerService,
                                   final RunMode runMode) throws SchedulerException {
        quartz.setJobFactory(new Quartz1JobFactory(schedulerService, runMode));
    }


    @Nullable
    Trigger getTrigger(final JobId jobId) {
        try {
            return getScheduler().getTrigger(jobId.toString(), QUARTZ_TRIGGER_GROUP);
        } catch (SchedulerException se) {
            logWarn("Error getting quartz trigger for '{}'", jobId, se);
            return null;
        }
    }

    @Nullable
    JobDetail getQuartzJob(final JobRunnerKey jobRunnerKey) {
        try {
            return getScheduler().getJobDetail(jobRunnerKey.toString(), QUARTZ_JOB_GROUP);
        } catch (SchedulerException se) {
            logWarn("Error getting quartz job details for '{}'", jobRunnerKey, se);
            return null;
        }
    }

    boolean hasAnyTriggers(final JobRunnerKey jobRunnerKey) {
        return getTriggersOfJob(jobRunnerKey).length > 0;
    }

    Collection<JobRunnerKey> getJobRunnerKeys() {
        try {
            return transform(asList(getScheduler().getJobNames(QUARTZ_JOB_GROUP)), new Function<String, JobRunnerKey>() {
                @SuppressWarnings("NullableProblems")  // Guava should not have stuck @Nullable on this...
                @Override
                public JobRunnerKey apply(String jobName) {
                    return JobRunnerKey.of(jobName);
                }
            });
        } catch (SchedulerException se) {
            throw unchecked("Could not get the triggers from Quartz", se);
        }
    }

    Trigger[] getTriggersOfJob(final JobRunnerKey jobRunnerKey) {
        try {
            return getScheduler().getTriggersOfJob(jobRunnerKey.toString(), QUARTZ_JOB_GROUP);
        } catch (SchedulerException se) {
            throw unchecked("Could not get the triggers from Quartz", se);
        }
    }

    boolean deleteTrigger(final JobId jobId) {
        try {
            return getScheduler().unscheduleJob(jobId.toString(), QUARTZ_TRIGGER_GROUP);
        } catch (SchedulerException se) {
            logWarn("Error removing Quartz trigger for '{}'", jobId, se);
            return false;
        }
    }

    boolean deleteJob(final JobRunnerKey jobRunnerKey) {
        try {
            return getScheduler().deleteJob(jobRunnerKey.toString(), QUARTZ_JOB_GROUP);
        } catch (SchedulerException se) {
            logWarn("Error removing Quartz job for '{}'", jobRunnerKey, se);
            return false;
        }
    }

    private void scheduleJob(final Trigger trigger) throws SchedulerServiceException {
        try {
            getScheduler().scheduleJob(trigger);
        } catch (SchedulerException se) {
            throw checked("Unable to create the Quartz trigger", se);
        }
    }


    void scheduleJob(final JobRunnerKey jobRunnerKey, final Trigger trigger) throws SchedulerServiceException {
        if (getQuartzJob(jobRunnerKey) != null) {
            trigger.setJobGroup(QUARTZ_JOB_GROUP);
            trigger.setJobName(jobRunnerKey.toString());
            scheduleJob(trigger);
            return;
        }

        try {
            final JobDetail quartzJob = new JobDetail();
            quartzJob.setGroup(QUARTZ_JOB_GROUP);
            quartzJob.setName(jobRunnerKey.toString());
            quartzJob.setJobClass(Quartz1Job.class);
            quartzJob.setDurability(false);
            getScheduler().scheduleJob(quartzJob, trigger);
        } catch (SchedulerException se) {
            throw checked("Unable to create the Quartz job and trigger", se);
        }
    }

    boolean unscheduleJob(final JobId jobId) {
        final Trigger trigger = getTrigger(jobId);
        if (trigger == null) {
            return false;
        }
        final JobRunnerKey jobRunnerKey = JobRunnerKey.of(trigger.getJobName());
        if (deleteTrigger(jobId) && !hasAnyTriggers(jobRunnerKey)) {
            deleteJob(jobRunnerKey);
        }
        return true;
    }

    void start() throws SchedulerServiceException {
        try {
            getScheduler().start();
        } catch (SchedulerException se) {
            throw checked("Quartz scheduler refused to start", se);
        }
    }

    void standby() throws SchedulerServiceException {
        try {
            getScheduler().standby();
        } catch (SchedulerException se) {
            throw checked("Quartz scheduler refused to enter standby mode", se);
        }
    }

    void shutdown() {
        try {
            getScheduler().shutdown();
        } catch (SchedulerException se) {
            LOG.error("Quartz scheduler did not shut down cleanly", se);
        }
    }

    /**
     * Returns the actual Quartz scheduler for direct access.
     * <p>
     * <strong>WARNING</strong>: This is only provided so that {@link Quartz1SchedulerService#getLocalQuartzScheduler()}
     * and {@link Quartz1SchedulerService#getClusteredQuartzScheduler()}} will work.  It will be removed in a future
     * release.
     * </p>
     *
     * @return the Quartz scheduler
     * @deprecated Since 1.0.  Provided as a last resort only; you should avoid accessing Quartz directly if possible.
     */
    @Deprecated
    Scheduler getQuartz() {
        return getScheduler();
    }

    private Scheduler getScheduler() {
        try {
            return quartzRef.get();
        } catch (LazyReference.InitializationException ex) {
            throw unchecked("Error creating underlying Quartz scheduler", ex.getCause());
        }
    }


    /**
     * Log a warning message, including the full stack trace iff DEBUG logging is enabled.
     *
     * @param message the log message, which should have exactly one {@code {}} placeholder for {@code arg}
     * @param arg     the argument for the message; typically a {@code JobId} or {@code JobRunnerKey}
     * @param e       the exception, which will be full stack traced if DEBUG logging is enabled; otherwise, appended
     *                to the message in {@code toString()} form.
     */
    private static void logWarn(String message, Object arg, Throwable e) {
        if (LOG.isDebugEnabled()) {
            LOG.warn(message, arg, e);
        } else {
            LOG.warn(message + ": {}", arg, e.toString());
        }
    }

    private static SchedulerServiceException checked(String message, Throwable e) {
        return new SchedulerServiceException(message, e);
    }

    private static SchedulerRuntimeException unchecked(String message, Throwable e) {
        return new SchedulerRuntimeException(message, e);
    }
}
