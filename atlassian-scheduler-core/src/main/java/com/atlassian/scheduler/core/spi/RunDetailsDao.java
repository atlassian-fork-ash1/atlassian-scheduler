package com.atlassian.scheduler.core.spi;

import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.status.RunDetails;

import javax.annotation.CheckForNull;

/**
 * Service provided by the host application to persist {@code RunDetails} objects.
 *
 * @since v1.0
 */
public interface RunDetailsDao {
    /**
     * Returns the result of the most recent attempt to run this job.
     *
     * @param jobId the job ID of interest
     * @return the result information for the most recent run attempt, or {@code null} if there
     * is no recorded run history for this job
     */
    @CheckForNull
    RunDetails getLastRunForJob(JobId jobId);

    /**
     * Returns the result of the most recent successful run of this job.
     *
     * @param jobId the job ID of interest
     * @return the result information for the most recent run attempt, or {@code null} if there
     * is no successful result recorded for this job
     */
    @CheckForNull
    RunDetails getLastSuccessfulRunForJob(JobId jobId);

    /**
     * Records the result of an attempt to run the specified job.
     *
     * @param jobId      the job ID of the job that the scheduler attempted to run
     * @param runDetails the result of the run
     */
    void addRunDetails(JobId jobId, RunDetails runDetails);
}
