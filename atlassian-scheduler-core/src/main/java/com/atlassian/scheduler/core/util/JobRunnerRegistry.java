package com.atlassian.scheduler.core.util;

import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.google.common.collect.ImmutableSet;

import javax.annotation.Nonnull;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static java.util.Objects.requireNonNull;

/**
 * Encapsulates the registration and retrieval of job runners.
 *
 * @since v1.0
 */
public class JobRunnerRegistry {
    private final ConcurrentMap<JobRunnerKey, JobRunner> jobRunnerRegistry = new ConcurrentHashMap<JobRunnerKey, JobRunner>();

    public void registerJobRunner(final JobRunnerKey jobRunnerKey, final JobRunner jobRunner) {
        jobRunnerRegistry.put(requireNonNull(jobRunnerKey, "jobRunnerKey"), requireNonNull(jobRunner, "jobRunner"));
    }

    public void unregisterJobRunner(final JobRunnerKey jobRunnerKey) {
        jobRunnerRegistry.remove(requireNonNull(jobRunnerKey, "jobRunnerKey"));
    }

    public JobRunner getJobRunner(final JobRunnerKey jobRunnerKey) {
        return jobRunnerRegistry.get(requireNonNull(jobRunnerKey, "jobRunnerKey"));
    }

    @Nonnull
    public Set<JobRunnerKey> getRegisteredJobRunnerKeys() {
        return ImmutableSet.copyOf(jobRunnerRegistry.keySet());
    }
}
