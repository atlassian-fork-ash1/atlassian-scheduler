/**
 * Functionality that must be provided by the host application.
 */
@ParametersAreNonnullByDefault package com.atlassian.scheduler.core.spi;

import javax.annotation.ParametersAreNonnullByDefault;