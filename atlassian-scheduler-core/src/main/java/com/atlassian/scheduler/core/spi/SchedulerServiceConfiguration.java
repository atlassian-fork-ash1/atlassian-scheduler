package com.atlassian.scheduler.core.spi;

import com.atlassian.scheduler.config.Schedule;

import javax.annotation.Nullable;
import java.util.TimeZone;

/**
 * Allows the host application to supply configuration parameters to the scheduler services.
 *
 * @since v1.0
 */
public interface SchedulerServiceConfiguration {
    /**
     * Returns the default {@code TimeZone} to use when scheduling a job to run according to a
     * {@link Schedule#forCronExpression(String) cron expression} when no specific time zone is
     * provided.
     * <p>
     * The value is checked when the job is scheduled and treated as if it were given explicitly.
     * As a result, if the host application is configured to use a new default time zone, then this
     * will be used for jobs that are scheduled after the change, but existing jobs will continue
     * to use the original setting.
     * </p>
     *
     * @return the default time zone to use for cron expression schedules; if the configuration
     * object returns {@code null}, then {@link TimeZone#getDefault()} is assumed
     */
    @Nullable
    TimeZone getDefaultTimeZone();
}
