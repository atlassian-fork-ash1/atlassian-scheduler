package com.atlassian.scheduler.core;

import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobId;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static com.atlassian.scheduler.core.Constants.JOB_ID;
import static com.atlassian.scheduler.core.LifecycleAwareSchedulerService.State.SHUTDOWN;
import static com.atlassian.scheduler.core.LifecycleAwareSchedulerService.State.STANDBY;
import static com.atlassian.scheduler.core.LifecycleAwareSchedulerService.State.STARTED;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class AbstractSchedulerServiceTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @SuppressWarnings("unchecked")
    @Test
    public void testWaitForScheduledJobsToCompleteWithTimeoutSuccessful() throws Exception {
        final AbstractSchedulerService schedulerService = new SchedulerServiceFixture();
        final long start = System.currentTimeMillis();
        assertThat("should go idle", schedulerService.waitUntilIdle(1000L, MILLISECONDS), is(true));
        assertThat("should detect idle quickly, not take most or all of the whole timeout",
                System.currentTimeMillis() - start, lessThan(300L));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void testWaitForScheduledJobsToCompleteWithTimeoutUnsuccessful() throws Exception {
        final SchedulerServiceFixture schedulerService = spy(new SchedulerServiceFixture());
        schedulerService.hackAwaitNanos();  // Force instant 30ms decrement per call
        assertThat(schedulerService.enterJob(JOB_ID, mock(RunningJob.class)), nullValue());

        assertThat("should time out", schedulerService.waitUntilIdle(100L, MILLISECONDS), is(false));

        // 100 -> 70 -> 40 -> 10 -> -20 => Timeout!
        verify(schedulerService, times(4)).awaitNanos(anyLong());
    }

    @SuppressWarnings("unchecked")
    @Test(expected = InterruptedException.class)
    public void testWaitForScheduledJobsToCompleteWithTimeoutInterrupted() throws Exception {
        final AbstractSchedulerService schedulerService = spy(new SchedulerServiceFixture());
        assertThat(schedulerService.enterJob(JOB_ID, mock(RunningJob.class)), nullValue());

        boolean hadToCallFail = false;
        try {
            Thread.currentThread().interrupt();
            final boolean result = schedulerService.waitUntilIdle(50L, MILLISECONDS);
            hadToCallFail = true;
            fail("Expected an InterruptedException but got result=" + result);
        } finally {
            // Don't leave the interrupted state polluted by the test
            final boolean threadWasInterrupted = Thread.interrupted();
            if (!hadToCallFail) {
                assertThat("Interrupted flag should have already been clear", threadWasInterrupted, is(false));
            }
            verify(schedulerService).awaitNanos(anyLong());
        }
    }

    @Test
    public void testWaitForScheduledJobsToCompleteWithTimeoutInterruptedButAlreadyIdle() throws Exception {
        final AbstractSchedulerService schedulerService = spy(new SchedulerServiceFixture());
        try {
            Thread.currentThread().interrupt();
            assertThat("should succeed", schedulerService.waitUntilIdle(50L, MILLISECONDS), is(true));
            assertThat("should still be interrupted", Thread.interrupted(), is(true));

            verify(schedulerService, never()).awaitNanos(anyLong());
        } finally {
            // Don't leave the interrupted state polluted by the test
            Thread.interrupted();
        }

    }

    @Test
    public void testEnterLeaveJob() {
        final AbstractSchedulerService schedulerService = new SchedulerServiceFixture();
        final RunningJob job1 = mock(RunningJob.class);
        final RunningJob job2 = mock(RunningJob.class);
        final JobId otherJobId = JobId.of("Some other job ID");
        final RunningJob otherJob = mock(RunningJob.class);

        assertThat("No jobs initially", schedulerService.getLocallyRunningJobs(), hasSize(0));
        assertThat("Enter other job", schedulerService.enterJob(otherJobId, otherJob), nullValue());
        assertThat("Entered other job", schedulerService.getLocallyRunningJobs(), contains(sameInstance(otherJob)));

        assertFailedLeave("Job is not running", schedulerService, job1);
        assertThat("Successful first entry when idle", schedulerService.enterJob(JOB_ID, job1), nullValue());
        assertThat(schedulerService.getLocallyRunningJobs(), containsInAnyOrder(job1, otherJob));

        assertFailedLeave("Wrong job specified", schedulerService, job2);
        assertThat(schedulerService.getLocallyRunningJobs(), containsInAnyOrder(job1, otherJob));

        assertThat("Unsuccessful re-entry", schedulerService.enterJob(JOB_ID, job1), sameInstance(job1));
        assertThat("Unsuccessful entry of second request", schedulerService.enterJob(JOB_ID, job2), sameInstance(job1));
        assertThat(schedulerService.getLocallyRunningJobs(), containsInAnyOrder(job1, otherJob));
        schedulerService.leaveJob(otherJobId, otherJob);  // successful
        assertThat("Left other job", schedulerService.getLocallyRunningJobs(), contains(job1));

        schedulerService.leaveJob(JOB_ID, job1);  // successful
        assertThat("Left job1", schedulerService.getLocallyRunningJobs(), hasSize(0));
        assertFailedLeave("Unsuccessful re-leave", schedulerService, job1);
        assertThat("Successful second entry when idle", schedulerService.enterJob(JOB_ID, job2), nullValue());
        assertThat("Left other job", schedulerService.getLocallyRunningJobs(), contains(job2));
    }

    static void assertFailedLeave(final String reason, final AbstractSchedulerService schedulerService, final RunningJob job) {
        try {
            schedulerService.leaveJob(JOB_ID, job);
            fail("Expected unsuccessful leaveJob because " + reason);
        } catch (IllegalStateException ise) {
            assertThat(ise.getMessage(), containsString("Invalid call to leaveJob"));
        }
    }

    @Test
    public void testLifecycle() throws SchedulerServiceException {
        final SchedulerServiceFixture schedulerService = new SchedulerServiceFixture();

        schedulerService.assertState(0, 0, 0, STANDBY);
        schedulerService.standby();
        schedulerService.assertState(0, 0, 0, STANDBY);

        schedulerService.start();
        schedulerService.assertState(1, 0, 0, STARTED);
        schedulerService.start();
        schedulerService.assertState(1, 0, 0, STARTED);

        schedulerService.standby();
        schedulerService.assertState(1, 1, 0, STANDBY);
        schedulerService.standby();
        schedulerService.assertState(1, 1, 0, STANDBY);

        schedulerService.start();
        schedulerService.assertState(2, 1, 0, STARTED);
        schedulerService.start();
        schedulerService.assertState(2, 1, 0, STARTED);

        schedulerService.shutdown();
        schedulerService.assertState(2, 1, 1, SHUTDOWN);
        schedulerService.shutdown();
        schedulerService.assertState(2, 1, 1, SHUTDOWN);

        try {
            schedulerService.start();
        } catch (SchedulerServiceException sse) {
            assertThat(sse.getMessage(), containsString("shut down"));
            schedulerService.assertState(2, 1, 1, SHUTDOWN);
        }

        try {
            schedulerService.standby();
        } catch (SchedulerServiceException sse) {
            assertThat(sse.getMessage(), containsString("shut down"));
            schedulerService.assertState(2, 1, 1, SHUTDOWN);
        }
    }
}
