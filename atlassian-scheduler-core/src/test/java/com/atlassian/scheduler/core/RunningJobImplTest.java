package com.atlassian.scheduler.core;

import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.core.impl.RunningJobImpl;
import org.junit.Test;

import java.util.Date;

import static com.atlassian.scheduler.core.Constants.JOB_ID;
import static com.atlassian.scheduler.core.Constants.KEY;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

/**
 * @since v1.0
 */
@SuppressWarnings({"ResultOfObjectAllocationIgnored", "ConstantConditions"})
public class RunningJobImplTest {
    @Test(expected = NullPointerException.class)
    public void testStartTimeNull() {
        new RunningJobImpl(null, JOB_ID, JobConfig.forJobRunnerKey(KEY));
    }

    @Test(expected = NullPointerException.class)
    public void testJobIdNull() {
        new RunningJobImpl(new Date(), null, JobConfig.forJobRunnerKey(KEY));
    }

    @Test(expected = NullPointerException.class)
    public void testJobConfigNull() {
        new RunningJobImpl(new Date(), JOB_ID, null);
    }

    @Test
    public void testValues() {
        final Date startTime = new Date();
        final long originalTime = startTime.getTime();
        final JobConfig config = JobConfig.forJobRunnerKey(KEY);
        final RunningJob job = new RunningJobImpl(startTime, JOB_ID, config);

        assertThat(job.getStartTime(), equalTo(startTime));
        assertThat(job.getJobId(), equalTo(JOB_ID));
        assertThat(job.getJobConfig(), sameInstance(config));

        // Date copy checks
        startTime.setTime(42L);
        assertThat(job.getStartTime().getTime(), equalTo(originalTime));
        job.getStartTime().setTime(42L);
        assertThat(job.getStartTime().getTime(), equalTo(originalTime));
    }

    @Test
    public void testCancelFlag() {
        final JobConfig config = JobConfig.forJobRunnerKey(KEY);
        final RunningJob job = new RunningJobImpl(new Date(), JOB_ID, config);

        assertThat("Not cancelled initially", job.isCancellationRequested(), is(false));

        job.cancel();
        assertThat("Cancelled once requested", job.isCancellationRequested(), is(true));

        job.cancel();
        assertThat("Redundant cancel() has no effect", job.isCancellationRequested(), is(true));
    }
}
