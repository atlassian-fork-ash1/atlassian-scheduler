package com.atlassian.scheduler.core.status;

import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.scheduler.core.AbstractSchedulerService;
import com.atlassian.scheduler.core.util.ParameterMapSerializer;
import com.atlassian.scheduler.status.JobDetails;
import org.junit.Test;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Date;

import static com.atlassian.scheduler.config.RunMode.RUN_LOCALLY;
import static com.atlassian.scheduler.config.RunMode.RUN_ONCE_PER_CLUSTER;
import static com.atlassian.scheduler.config.Schedule.forInterval;
import static com.atlassian.scheduler.config.Schedule.runOnce;
import static com.atlassian.scheduler.core.Constants.BYTES_DEADF00D;
import static com.atlassian.scheduler.core.Constants.BYTES_PARAMETERS;
import static com.atlassian.scheduler.core.Constants.JOB_ID;
import static com.atlassian.scheduler.core.Constants.KEY;
import static com.atlassian.scheduler.core.Constants.PARAMETERS;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v1.0
 */
@SuppressWarnings({"ResultOfObjectAllocationIgnored", "ConstantConditions"})
public class AbstractJobDetailsFactoryTest {
    @Test(expected = NullPointerException.class)
    public void testSchedulerServiceIsNull() {
        new Fixture(null, 1L, KEY, runOnce(null), new Date(), null);
    }

    @Test(expected = NullPointerException.class)
    public void testJobIdNull() {
        new Fixture(service(), 1L, KEY, runOnce(null), new Date(), null)
                .buildJobDetails(null, 1L, RUN_LOCALLY);
    }

    @Test(expected = NullPointerException.class)
    public void testJobDataNull() {
        new Fixture(service(), null, KEY, runOnce(null), new Date(), null)
                .buildJobDetails(JOB_ID, null, RUN_LOCALLY);
    }

    @Test(expected = NullPointerException.class)
    public void testRunModeNull() {
        new Fixture(service(), null, KEY, runOnce(null), new Date(), null)
                .buildJobDetails(JOB_ID, 1L, null);
    }

    @Test(expected = NullPointerException.class)
    public void testJobRunnerKeyNull() {
        new Fixture(service(), 1L, null, runOnce(null), new Date(), null)
                .buildJobDetails(JOB_ID, 1L, RUN_LOCALLY);
    }

    @Test(expected = NullPointerException.class)
    public void testScheduleNull() {
        new Fixture(service(), 1L, KEY, null, new Date(), null)
                .buildJobDetails(JOB_ID, 1L, RUN_LOCALLY);
    }

    @Test
    public void testNoJobRunner() {
        final AbstractSchedulerService service = service();
        final Date now = new Date();
        final Fixture fixture = new Fixture(service, 1L, KEY, runOnce(now), null, null);
        final JobDetails jobDetails = fixture.buildJobDetails(JOB_ID, 1L, RUN_LOCALLY);

        assertThat(jobDetails.getJobId(), is(JOB_ID));
        assertThat(jobDetails.getJobRunnerKey(), is(KEY));
        assertThat(jobDetails.getNextRunTime(), nullValue());
        assertThat(jobDetails.getRunMode(), is(RUN_LOCALLY));
        assertThat(jobDetails.getSchedule(), is(runOnce(now)));
        assertThat(jobDetails.isRunnable(), is(false));
    }

    @Test
    public void testJobRunnerClassLoaderFails() {
        final AbstractSchedulerService service = service();
        final Date now = new Date();
        final Fixture fixture = new Fixture(service, 2L, KEY, runOnce(now), null, BYTES_DEADF00D);
        when(service.getJobRunner(KEY)).thenReturn(mock(JobRunner.class));

        final JobDetails jobDetails = fixture.buildJobDetails(JOB_ID, 2L, RUN_LOCALLY);

        assertThat(jobDetails.getJobId(), is(JOB_ID));
        assertThat(jobDetails.getJobRunnerKey(), is(KEY));
        assertThat(jobDetails.getNextRunTime(), nullValue());
        assertThat(jobDetails.getRunMode(), is(RUN_LOCALLY));
        assertThat(jobDetails.getSchedule(), is(runOnce(now)));
        assertThat(jobDetails.isRunnable(), is(false));
    }

    @Test
    public void testValues() {
        final AbstractSchedulerService service = service();
        final Date now = new Date();
        final Fixture fixture = new Fixture(service, 3L, KEY, forInterval(60000L, now), null, BYTES_PARAMETERS);
        when(service.getJobRunner(KEY)).thenReturn(mock(JobRunner.class));

        final JobDetails jobDetails = fixture.buildJobDetails(JOB_ID, 3L, RUN_ONCE_PER_CLUSTER);

        assertThat(jobDetails.getJobId(), is(JOB_ID));
        assertThat(jobDetails.getJobRunnerKey(), is(KEY));
        assertThat(jobDetails.getNextRunTime(), nullValue());
        assertThat(jobDetails.getRunMode(), is(RUN_ONCE_PER_CLUSTER));
        assertThat(jobDetails.getSchedule(), is(forInterval(60000L, now)));
        assertThat(jobDetails.isRunnable(), is(true));
        assertThat(jobDetails.getParameters(), is(PARAMETERS));
    }

    static AbstractSchedulerService service() {
        final AbstractSchedulerService schedulerService = mock(AbstractSchedulerService.class);
        when(schedulerService.getParameterMapSerializer()).thenReturn(new ParameterMapSerializer());
        return schedulerService;
    }

    @SuppressWarnings({"AssignmentToDateFieldFromParameter", "AssignmentToCollectionOrArrayFieldFromParameter"})
    static class Fixture extends AbstractJobDetailsFactory<Long> {
        private final Long expected;
        private final JobRunnerKey jobRunnerKey;
        private final Schedule schedule;
        private final Date nextRunTime;
        private final byte[] parameters;

        public Fixture(AbstractSchedulerService schedulerService, Long expected, JobRunnerKey jobRunnerKey,
                       Schedule schedule, Date nextRunTime, byte[] parameters) {
            super(schedulerService);
            this.expected = expected;
            this.jobRunnerKey = jobRunnerKey;
            this.schedule = schedule;
            this.nextRunTime = nextRunTime;
            this.parameters = parameters;
        }

        @Nonnull
        @Override
        protected JobRunnerKey getJobRunnerKey(Long jobData) {
            assertSame(expected, jobData);
            return jobRunnerKey;
        }

        @Nonnull
        @Override
        protected Schedule getSchedule(Long jobData) {
            assertSame(expected, jobData);
            return schedule;
        }

        @Nullable
        @Override
        protected Date getNextRunTime(Long jobData) {
            assertSame(expected, jobData);
            return nextRunTime;
        }

        @Nullable
        @Override
        protected byte[] getSerializedParameters(Long jobData) {
            assertSame(expected, jobData);
            return parameters;
        }
    }
}



