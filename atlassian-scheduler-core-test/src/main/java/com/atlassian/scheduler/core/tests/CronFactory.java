package com.atlassian.scheduler.core.tests;

import org.joda.time.DateTimeZone;

import javax.annotation.Nullable;
import java.util.Date;

/**
 * Generate cron expression facades for a test.
 *
 * @since v1.5
 */
public interface CronFactory {
    void parseAndDiscard(String cronExpression);

    CronExpressionAdapter parse(String cronExpression);

    CronExpressionAdapter parse(String cronExpression, DateTimeZone zone);

    interface CronExpressionAdapter {
        boolean isSatisfiedBy(final Date date);

        @Nullable
        Date nextRunTime(final Date dateTime);
    }
}
