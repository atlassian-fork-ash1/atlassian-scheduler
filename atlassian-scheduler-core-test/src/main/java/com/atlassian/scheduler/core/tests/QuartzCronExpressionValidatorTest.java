package com.atlassian.scheduler.core.tests;

import com.atlassian.scheduler.cron.CronSyntaxException;
import com.atlassian.scheduler.cron.ErrorCode;
import org.junit.Test;

import static com.atlassian.scheduler.cron.ErrorCode.INVALID_NAME;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * Quartz has so many stupid bugs in it (both in 1.x and 2.x) that we have little choice but to hack up
 * the tests with a bunch of special cases.  This tries to encapsulate all of that ugliness.
 *
 * @since v1.4
 */
public abstract class QuartzCronExpressionValidatorTest extends CronExpressionValidatorTest {
    /**
     * Exempts the Quartz validator from reporting error offsets correctly as long as it
     * reports {@code -1}.
     * <p>
     * See {@link com.atlassian.scheduler.core.util.QuartzParseExceptionMapper} for the sad explanation.
     * </p>
     */
    @Override
    protected boolean verifyErrorOffset(int expected, CronSyntaxException cse) {
        return cse.getErrorOffset() == -1 || super.verifyErrorOffset(expected, cse);
    }

    /**
     * Allow Quartz validator to fail to detect this error.
     * <p>
     * There are several cases where the Quartz cron expression parser forgives errors in the syntax that it
     * really should not permit.
     * </p>
     */
    @Override
    protected void assertErrorQuartzExempt(String cronExpression, ErrorCode errorCode, String message, String value,
                                           int errorOffset, String whyQuartzIsWrong) {
        try {
            validator.validate(cronExpression);
            System.err.println("Quartz thinks '" + cronExpression + "' is valid: " + whyQuartzIsWrong);
        } catch (CronSyntaxException cse) {
            assertThat(cse, exception(cronExpression, errorCode, message, value, errorOffset));
            fail("Quartz used to think '" + cronExpression + "' was valid, but now correctly identifies the problem!");
        }
    }

    /**
     * Quartz's assumption that names are always 3 chars long without checking means that this causes
     * more characters to get gobbled up as part of the name than should be.
     */
    @Override
    @Test
    public void testNameRangesWrongLength() {
        assertInvalidNameDayOfWeek("0 0 0 ? 1 M-FRI", "M-F", 10);
        assertInvalidNameDayOfWeek("0 0 0 ? 1 MO-FRI", "MO-", 10);
        assertInvalidNameRange("0 0 0 ? 1 MON-F", 14);
        assertInvalidNameRange("0 0 0 ? 1 MON-FR", 14);
        assertInvalidName("0 0 0 WL 1 ?", "WL", 6);
    }

    /**
     * Quartz's assumption that names are always 3 chars long without checking means that this causes
     * a {@code StringIndexOutOfBoundsException} and gets reported as a general failure.
     * <p>
     * We work backwards to try to tell what went wrong, but the logic for this is not perfect and
     * we cannot tell what field was being checked at the time.  We can't assume it is always the
     * day-of-week at the end of the expression, because Quartz tokenizes the expression into fields
     * beforehand, so it could just as easily happen to the day-of-month.  All we can do is report
     * {@code INVALID_NAME} to be as friendly/helpful as possible.
     * </p><p>
     * That's better than nothing, I guess, but the main test does the correct assertion.
     * </p>
     */
    @Override
    @Test
    public void testTruncatedExpression() {
        assertError("0 0 0 ? 1 NO", INVALID_NAME, "Invalid name: 'NO'", "NO", 10);
    }
}
