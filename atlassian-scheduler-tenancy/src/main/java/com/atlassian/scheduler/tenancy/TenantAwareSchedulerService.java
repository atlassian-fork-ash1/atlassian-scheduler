package com.atlassian.scheduler.tenancy;

import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.core.DelegatingSchedulerService;
import com.atlassian.scheduler.core.LifecycleAwareSchedulerService;
import com.atlassian.scheduler.core.RunningJob;
import com.atlassian.tenancy.api.TenantAccessor;
import com.google.common.collect.Iterables;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

/**
 * Decorates a lifecycle-aware scheduler service with awareness of application tenancy.
 * <p>
 * Any attempt to schedule a job with no tenant available results in an exception.
 * </p><p>
 * Implemention note: This provides the full {@link LifecycleAwareSchedulerService} interface
 * rather than the {@link SchedulerService} subset so that the raw, non-tenant-aware delegate
 * can be completely hidden away at construction time without losing the ability to control
 * its lifecycle.  For example, in JIRA (which uses {@code PicoContainer}, instead of registering
 * a {@code FooSchedulerService} directly as the {@code LifecycleAwareSchedulerService}
 * implementation, we might register something like this instead:
 * </p>
 * <pre><code>
 * // Registrations:
 * //   FooConfiguration =&gt; JiraFooConfiguration
 * //   RunDetailsDao =&gt; OfbizRunDetailsDao
 * //   TenantAccessor =&gt; DefaultJiraTenantAccessor
 * public class JiraFooSchedulerService extends TenantAwareSchedulerService
 * {
 *     public JiraFooSchedulerService(FooConfiguration config, RunDetailsDao runDetailsDao,
 *              TenantAccessor tenantAccessor)
 *     {
 *         super(new FooSchedulerService(config, runDetailsDao), tenantAccessor);
 *     }
 * }
 * </code></pre>
 * <p>
 * It is not, of course, absolutely necessary to do this.  We could register {@code FooSchedulerService} directly
 * and use an explicit constructor specification for {@code TenantAwareSchedulerService} that will use
 * {@code FooSchedulerService} as the parameter's key instead of the {@code LifecycleAwareSchedulerService}
 * key that Pico would find by reflection.  However, there doesn't seem to be any real benefit to the approach
 * unless there are legitimate reasons to access the {@code FooSchedulerService} directly, which would bypass
 * the tenant-awareness.
 * </p>
 *
 * @since v1.7.0
 */
public class TenantAwareSchedulerService extends DelegatingSchedulerService implements LifecycleAwareSchedulerService {
    private final LifecycleAwareSchedulerService delegate;
    private final TenantAccessor tenantAccessor;

    public TenantAwareSchedulerService(LifecycleAwareSchedulerService delegate, TenantAccessor tenantAccessor) {
        super(delegate);
        this.delegate = delegate;
        this.tenantAccessor = tenantAccessor;
    }

    @Override
    public void scheduleJob(JobId jobId, JobConfig jobConfig) throws SchedulerServiceException {
        assertTenantAvailable();
        super.scheduleJob(jobId, jobConfig);
    }

    @Nonnull
    @Override
    public JobId scheduleJobWithGeneratedId(JobConfig jobConfig) throws SchedulerServiceException {
        assertTenantAvailable();
        return super.scheduleJobWithGeneratedId(jobConfig);
    }

    @Override
    public void start() throws SchedulerServiceException {
        delegate.start();
    }

    @Override
    public void standby() throws SchedulerServiceException {
        delegate.standby();
    }

    @Override
    public void shutdown() {
        delegate.shutdown();
    }

    @Override
    @Nonnull
    public Collection<RunningJob> getLocallyRunningJobs() {
        return delegate.getLocallyRunningJobs();
    }

    @Override
    public boolean waitUntilIdle(long timeout, TimeUnit units) throws InterruptedException {
        return delegate.waitUntilIdle(timeout, units);
    }

    @Override
    @Nonnull
    public State getState() {
        return delegate.getState();
    }

    private void assertTenantAvailable() {
        if (Iterables.isEmpty(tenantAccessor.getAvailableTenants())) {
            throw new IllegalStateException("You are not allowed to schedule jobs before a tenant is available.");
        }
    }
}
