package com.atlassian.scheduler.tenancy;

import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.RunMode;
import com.atlassian.scheduler.core.LifecycleAwareSchedulerService;
import com.atlassian.tenancy.api.Tenant;
import com.atlassian.tenancy.api.TenantAccessor;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class TenantAwareSchedulerServiceTest {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    TenantAccessor tenantAccessor;
    @Mock
    Tenant tenant;
    @Mock
    LifecycleAwareSchedulerService delegate;

    TenantAwareSchedulerService schedulerService;

    @Before
    public void setUp() {
        schedulerService = new TenantAwareSchedulerService(delegate, tenantAccessor);
    }

    @Test
    public void testScheduleJobOkWhenTenanted() throws SchedulerServiceException {
        when(tenantAccessor.getAvailableTenants()).thenReturn(ImmutableList.of(tenant));
        final JobConfig jobConfig = JobConfig.forJobRunnerKey(JobRunnerKey.of("test-key"))
                .withRunMode(RunMode.RUN_LOCALLY);
        schedulerService.scheduleJob(JobId.of("jobId"), jobConfig);
        verify(delegate).scheduleJob(JobId.of("jobId"), jobConfig);
    }

    @Test
    public void testScheduleJobWithGeneratedIdOkWhenTenanted() throws SchedulerServiceException {
        when(tenantAccessor.getAvailableTenants()).thenReturn(ImmutableList.of(tenant));
        final JobConfig jobConfig = JobConfig.forJobRunnerKey(JobRunnerKey.of("test-key"))
                .withRunMode(RunMode.RUN_LOCALLY);
        schedulerService.scheduleJobWithGeneratedId(jobConfig);
        verify(delegate).scheduleJobWithGeneratedId(jobConfig);
    }

    @Test
    public void testScheduleJobThrowsIllegalStateExceptionWhenTenantless() throws SchedulerServiceException {
        when(tenantAccessor.getAvailableTenants()).thenReturn(ImmutableList.<Tenant>of());
        final JobConfig jobConfig = JobConfig.forJobRunnerKey(JobRunnerKey.of("test-key"))
                .withRunMode(RunMode.RUN_LOCALLY);

        try {
            schedulerService.scheduleJob(JobId.of("jobId"), jobConfig);
            fail("Expected an IllegalStateException");
        } catch (IllegalStateException ise) {
            assertThat(ise.getMessage(), containsString("not allowed to schedule jobs before"));
            verifyZeroInteractions(delegate);
        }
    }

    @Test
    public void testScheduleJobWithGeneratedIdThrowsIllegalStateExceptionWhenTenantless() throws SchedulerServiceException {
        when(tenantAccessor.getAvailableTenants()).thenReturn(ImmutableList.<Tenant>of());
        final JobConfig jobConfig = JobConfig.forJobRunnerKey(JobRunnerKey.of("test-key"))
                .withRunMode(RunMode.RUN_LOCALLY);
        try {
            schedulerService.scheduleJobWithGeneratedId(jobConfig);
            fail("Expected an IllegalStateException");
        } catch (IllegalStateException ise) {
            assertThat(ise.getMessage(), containsString("not allowed to schedule jobs before"));
            verifyZeroInteractions(delegate);
        }
    }

}
