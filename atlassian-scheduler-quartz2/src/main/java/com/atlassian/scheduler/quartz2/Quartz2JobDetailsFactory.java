package com.atlassian.scheduler.quartz2;

import com.atlassian.scheduler.SchedulerRuntimeException;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.scheduler.core.status.AbstractJobDetailsFactory;
import org.quartz.CronTrigger;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Date;

import static com.atlassian.scheduler.quartz2.Quartz2SchedulerFacade.QUARTZ_PARAMETERS_KEY;

/**
 * Creates {@code JobDetails} objects out of Quartz 2.x {@code Trigger}s.
 *
 * @since v1.0
 */
public class Quartz2JobDetailsFactory extends AbstractJobDetailsFactory<Trigger> {
    Quartz2JobDetailsFactory(Quartz2SchedulerService schedulerService) {
        super(schedulerService);
    }

    @Nonnull
    @Override
    protected JobRunnerKey getJobRunnerKey(Trigger trigger) {
        return JobRunnerKey.of(trigger.getJobKey().getName());
    }

    @Nonnull
    @Override
    protected Schedule getSchedule(Trigger trigger) {
        if (trigger instanceof CronTrigger) {
            final CronTrigger cron = (CronTrigger) trigger;
            return Schedule.forCronExpression(cron.getCronExpression(), cron.getTimeZone());
        }
        if (trigger instanceof SimpleTrigger) {
            final SimpleTrigger simple = (SimpleTrigger) trigger;
            return Schedule.forInterval(simple.getRepeatInterval(), simple.getStartTime());
        }
        throw new SchedulerRuntimeException("The job with jobId '" + trigger.getKey().getName() +
                "' has an unsupported trigger class: " + trigger.getClass().getName());
    }

    @Nullable
    @Override
    protected byte[] getSerializedParameters(Trigger trigger) {
        return (byte[]) trigger.getJobDataMap().get(QUARTZ_PARAMETERS_KEY);
    }

    @Nullable
    @Override
    protected Date getNextRunTime(Trigger trigger) {
        return trigger.getNextFireTime();
    }
}
