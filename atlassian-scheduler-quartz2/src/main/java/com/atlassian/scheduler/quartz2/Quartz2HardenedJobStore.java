package com.atlassian.scheduler.quartz2;

import org.quartz.JobDetail;
import org.quartz.JobPersistenceException;
import org.quartz.impl.jdbcjobstore.JobStoreTX;
import org.quartz.spi.OperableTrigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.sql.Connection;
import java.util.concurrent.locks.ReentrantLock;

import static com.atlassian.scheduler.core.util.LogWarn.logWarn;

/**
 * Quartz breaks if a bad job class is seen during scheduler recovery (at startup),
 * leading to the scheduler failing to start.  This patches the normal {@code JobStoreTX}
 * to avoid the problem by logging a warning instead of failing to start the scheduler.
 */
public class Quartz2HardenedJobStore extends JobStoreTX {
    private static final Logger LOG = LoggerFactory.getLogger(Quartz2HardenedJobStore.class);

    private final ReentrantLock recoverJobsLock = new ReentrantLock();

    @Override
    protected void recoverJobs() throws JobPersistenceException {
        recoverJobsLock.lock();
        try {
            super.recoverJobs();
        } finally {
            recoverJobsLock.unlock();
        }
    }

    @Override
    protected void storeTrigger(Connection conn, OperableTrigger newTrigger, @Nullable JobDetail job,
                                boolean replaceExisting, String state, boolean forceState, boolean recovering)
            throws JobPersistenceException {
        try {
            super.storeTrigger(conn, newTrigger, job, replaceExisting, state, forceState, recovering);
        } catch (JobPersistenceException jpe) {
            // We don't actually expect this lock to ever be contended.  We just need to know if this thread
            // has traversed "recoverJobs" on the way here, and lock ownership is a convenient way to find out.
            if (!recoverJobsLock.isHeldByCurrentThread()) {
                throw jpe;
            }
            logWarn(LOG, "Caught an exception storing trigger during scheduler recovery", jpe);
        }
    }
}
