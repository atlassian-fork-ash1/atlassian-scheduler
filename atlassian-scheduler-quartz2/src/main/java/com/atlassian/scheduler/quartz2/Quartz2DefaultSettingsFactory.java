package com.atlassian.scheduler.quartz2;

import com.google.common.collect.ImmutableMap;
import org.quartz.simpl.RAMJobStore;
import org.quartz.simpl.SimpleThreadPool;

import java.util.Map;
import java.util.Properties;

/**
 * Generates an initial {@code Properties} object that a {@code Quartz2ConfigurationSettings} can use
 * as a starting point for further configuration.
 *
 * @since v1.3
 */
public class Quartz2DefaultSettingsFactory {
    private static final ImmutableMap<String, String> DEFAULT_LOCAL_CONFIG = ImmutableMap.<String, String>builder()
            .put("org.quartz.jobStore.class", RAMJobStore.class.getName())
            .put("org.quartz.scheduler.instanceName", "atlassian-scheduler-quartz2.local")
            .put("org.quartz.scheduler.skipUpdateCheck", "true")
            .put("org.quartz.threadPool.class", SimpleThreadPool.class.getName())
            .put("org.quartz.threadPool.threadCount", "4")
            .put("org.quartz.threadPool.threadPriority", "4")
            .build();

    private static final ImmutableMap<String, String> DEFAULT_CLUSTERED_CONFIG = ImmutableMap.<String, String>builder()
            .put("org.quartz.jobStore.class", Quartz2HardenedJobStore.class.getName())
            .put("org.quartz.jobStore.isClustered", "true")
            .put("org.quartz.scheduler.instanceName", "atlassian-scheduler-quartz2.clustered")
            .put("org.quartz.scheduler.skipUpdateCheck", "true")
            .put("org.quartz.threadPool.class", SimpleThreadPool.class.getName())
            .put("org.quartz.threadPool.threadCount", "4")
            .put("org.quartz.threadPool.threadPriority", "4")
            .build();

    public static Properties getDefaultLocalSettings() {
        return toProperties(DEFAULT_LOCAL_CONFIG);
    }

    public static Properties getDefaultClusteredSettings() {
        return toProperties(DEFAULT_CLUSTERED_CONFIG);
    }

    private static Properties toProperties(final Map<String, String> defaultConfig) {
        final Properties config = new Properties();
        for (Map.Entry<String, String> entry : defaultConfig.entrySet()) {
            config.setProperty(entry.getKey(), entry.getValue());
        }
        return config;
    }
}

