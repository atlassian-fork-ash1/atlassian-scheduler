/**
 * Implements that {@code atlassian-scheduler} API using Quartz 2.x.
 */
@ParametersAreNonnullByDefault package com.atlassian.scheduler.quartz2;

import javax.annotation.ParametersAreNonnullByDefault;