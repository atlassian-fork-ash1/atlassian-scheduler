package com.atlassian.scheduler.quartz2;

import com.atlassian.scheduler.core.tests.CronExpressionHoursTest;

/**
 * @since v1.5
 */
public class Quartz2CronExpressionHoursTest extends CronExpressionHoursTest {
    public Quartz2CronExpressionHoursTest() {
        super(new Quartz2CronFactory());
    }
}
