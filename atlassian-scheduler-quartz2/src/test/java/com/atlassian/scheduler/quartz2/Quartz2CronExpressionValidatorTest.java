package com.atlassian.scheduler.quartz2;

import com.atlassian.scheduler.core.tests.QuartzCronExpressionValidatorTest;
import org.junit.Before;

/**
 * @since v1.4
 */
public class Quartz2CronExpressionValidatorTest extends QuartzCronExpressionValidatorTest {
    @Before
    public void setUp() {
        validator = new Quartz2CronExpressionValidator();
    }
}
