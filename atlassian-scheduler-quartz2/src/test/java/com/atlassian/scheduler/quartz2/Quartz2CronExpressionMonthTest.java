package com.atlassian.scheduler.quartz2;

import com.atlassian.scheduler.core.tests.CronExpressionMonthTest;

/**
 * @since v1.5
 */
public class Quartz2CronExpressionMonthTest extends CronExpressionMonthTest {
    public Quartz2CronExpressionMonthTest() {
        super(new Quartz2CronFactory());
    }
}
